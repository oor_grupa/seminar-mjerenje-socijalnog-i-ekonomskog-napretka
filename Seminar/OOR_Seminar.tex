\documentclass[lmodern, utf8, seminar, numeric]{feri}
\usepackage{booktabs}
\usepackage{url}
\usepackage{cite}

\raggedright					% bez punog poravnavanja, samo lijevo
\setlength{\parindent}{1em}		% uvlačenje ulomaka
\usepackage{indentfirst}		% uvlačenje prvog ulomka
\setlength{\parskip}{0.5em}		% razmak između ulomaka
\graphicspath{ {./slike/} }		% mapa sa slikama
\usepackage{float}				% za slike
\captionsetup[figure]{width = 0.9\textwidth, justification=centering, singlelinecheck=off} % tekst uz sliku
\usepackage{tabularx}
\captionsetup[table]{width = 0.9\textwidth, justification=centering, singlelinecheck=off} % tekst uz tablicu
\usepackage[unicode, colorlinks=true, hidelinks]{hyperref}
\usepackage{array}

\begin{document}

% Ukljuci literaturu u seminar
\nocite{*}

% TODO: Navedite naslov rada.
\title{Mjerenje socijalnog i ekonomskog napretka}

% TODO: Navedite vaše ime i prezime.
\author{Marko Bukal, Ivan Grubišić, Ante Tomić}

% TODO: Navedite ime i prezime voditelja.
\voditelj{doc. dr. sc. Siniša Šadek}

\maketitle

\tableofcontents


\chapter{Uvod}

Razvijen 1930-ih od strane tima s ekonomistom Simonom Kuznetsom na čelu, bruto domaći proizvod (BDP, eng. \emph{GDP - Gross Domestic Product}) je koncipiran kao odgovor na spoznaju da ograničene i fragmentirane gospodarstvene informacije predstavljaju izazov za vođenje politike tijekom Velike gospodarske krize u SAD-u.\citep{mepewb} 

BDP je definiran kao zbroj osobne potrošnje $C$, investicija $I$, državne potrošnje $G$ i neto trgovinske bilance, razlike izvoza $X$ i uvoza $M$, što se izražava formulom: $$ Y = C + I + G + (X - M) .$$
Napredak i blagostanje u nekoj državi često se procjenjuju usporedbom BDP-a, tj. zbroja godišnje novčane vrijednosti svih proizvedenih dobara i usluga. Međutim, BDP nikada nije bio namijenjen korištenju u tu svrhu. Podložan je produktivizmu i konzumerizmu, precjenjivanju proizvodnje i potrošnje dobara, i neodražavanju poboljšanja u blagostanju ljudi i okoliša. Ne razlikuje novac utrošen na proizvodnju od novca utrošenog za popravak negativnih ishoda prethodnih troškova.

Kao agregatna mjera gospodarske aktivnosti, BDP nije razvijen za mjerenje društvenog blagostanja, uračunavanje utjecaja na okoliš, raspodjele dohotka ili zdravlja i zadovoljstva ljudi. BDP također ne govori ništa o tome kako način na koji se trenutno proizvodi utječe na mogućnosti proizvodnje u budućnosti.

Simon Kuznets, izumitelj koncepta BDP-a, u svom prvom izvješću Kongresu SAD-a 1934. napominje: "Na umu se moraju imati razlike između količine i kvalitete rasta, između troškova i povrata, te između kratkog i dugog roka. Ciljevi za veći rast trebaju točno navesti više rasta čega i za što." \citep{wp_gdp}

Prikladna mjera napretka također mora uzeti u obzir ekološke prinose i mogućnosti prirode da pruža usluge. Te stvari su dio obuhvatnijeg ideala napretka koji proširuje tradicionalni fokus na sirovu industrijsku proizvodnju.


\chapter{Ograničenja BDP-a kao mjere napretka i dobrobiti}

Prije razmatranja drugih mogućnosti u vezi mjerenja društvenog i ekonomskog napretka, nabrojat ćemo najznačajnije nedostatke BDP-a koje je potrebno riješiti pri traćenju bolje alternativne mjere društvenog napretka. \citep{ieppmhw}
\begin{enumerate}
	\item BDP ne razlikuje čimbenike koji pridonose društvenom napretku i blagostanju od onih koji ih narušavaju i odražavaju njihovo opadanje. Elementarne nepogode, saniranje štete, rastave, kriminal i rat se pozitivno odražavaju na BDP.
	\item BDP kao mjera aktivnosti krivo se tumači kao mjera bogatstva. Viša razina rasta BDP-a može biti praćena povećanjem državnog duga, zaduživanjem kućanstava ili iscrpljivanjem prirodnih resursa.
	\item BDP pozitivno ubraja neke oblike gospodarske aktivnosti koje troše više kapitala nego što ga proizvode. Korištenje neobnovljivih resursa stvara ekonomski tok potrošnjom nenadomjestive vrijednosti. Rat povećava gospodarsku aktivnost aktivnostima koje zapravo uništavaju proizvode kao i druge prirodne i društvene oblike kapitala
	\item BDP ne razlikuje održive i neodržive aktivnosti. Iscrpljivanje prirodnih resursa i troškove kompenzacije štete prikazuje kao dobitke. Također, povećana potrošnja ne mora nužno odražavati veće ekonomsko blagostanje, kao što npr. smanjenje kvalitete vode u javnoj opskrbi povećava potražnju za skupljom vodom u bocama.
	\item BDP zanemaruje učinak nezaposlenosti na sigurnost i blagostanje ljudi.
	\item BDP ne uzima u obzir raspodjelu dohotka. Iznos prosječnog BDP-a po stanovniku može prikriti činjenicu da rast može negativan za značajan dio društva, dok je jako velik mali dio stanovništva.
	\item BDP ne razlikuje spekulativne dobitke na financijskim tržištima tijekom i stvarne dobitke od povećanja zaposlenosti, proizvodnje i osobne potrošnje.
	\item BDP ne uključuje netržišne aktivnosti unutar zajednice i kućanstva. Ne pridjeljuje nikakvu vrijednost radu u kućanstvu, odgoju i obrazovanju djece, volonterstvu i drugim dobrovoljnim aktivnostima koje koriste pojedincima i društvu u cjelini.
	\item BDP i druge mjere temeljene na cijeni podcjenjuju stvarna poboljšanja u kvaliteti života jer mjere samo cijenu dobara i usluga zanemarujući poboljšanja u kvaliteti proizvoda i života. Ti dobici dolaze od stvarnog napretka u društvom razvoju, organizaciji, znanosti i tehnologiji. 
\end{enumerate}


\chapter{Svojstva dobrih metrika napretka}

U protekla dva poglavlja uvidjeli smo kako je BDP zaživio, koja je bila njegova namjena te kako su se kasnije uvidjeli nedostatci tog indikatora ekonomskog napretka.
Razumljivo, kada su ekonomisti uvidjeli probleme s interpretiranjem BDP-a, odlučili su pronaći svojstva i karakteristike te nove mjere koja bi ili zamijenila ili nadopunila BDP.
Ipak, bilo bi nepravedno tvrditi da je BDP bio potpuno promašeni koncept. Pa tako, jedna od dobrih strana BDP-a je njegova jednostavnost i univerzalnost. BDP se vrlo jasno definira te prilično jednostavno računa te kao takav može poslužiti kao dobra brojka prilikom usporedbi dviju ekonomija. Ovo svojstva treba imati na umu kada ćemo opisivati alternativne indikatore napretka.
Nove, alternativne metrike trebale bi u svojim mjerenjima iskazivati sljedeće parametre:
\begin{itemize}
	\item ekonomski rast
	\item ekonomsko blagostanje
	\item društveni napredak
	\item održivi razvoj
	\item ljudsko blagostanje, tj. ukupna kvaliteta života.
\end{itemize}

Pod ekonomskim rastom podrazumijevamo povećanje nacionalne dobiti koju je do sada relativno dobro mjerio BDP.

Pojam ekonomsko blagostanje se odnosi na utjecaj ekonomskog rasta države na poboljšanje ekonomskog standarda ljudi. Vidjeli smo da BDP pokazuje produktivnu moć neke zemlje i da ta brojka ne mora nužno reflektirati sam standard ljudi. Pod standardom podrazumijevamo sve one usluge koje država može i želi pružati svojim građanima poput subvencioniranja i poboljšanja kvalitete javnog zdravstva, unaprjeđenja školstva na svim razinama, reformiranje birokracije s fokusom na pojednostavljenje, ali isto tako i smanjivanje investicija u vojnu industriju i naoružanje te smanjivanje troškova javne uprave kojima se dolazi do novca koji se opet može uložiti u nešto bitno građanima.

Socijalni napredak predstavlja poboljšanja u strukturalnim mogućnostima društva, odnosno povećan kapacitet za organizacijom, koordinacijom i kompleksnošću društva. Za razliku od pojma "rasta" koji se često smatra isključivo kvantitativnim ekonomskim pojmom, "napredak" označava poboljšanje društvene produktivnosti kroz strategije poput investiranja u ljudski kapital kroz edukaciju i trening, poboljšanje organizacije, napredak u znanosti te povećavanja protočnosti informacija među institucijama.

Prema definicije UN-a održivi razvoj je onaj razvoj koji zadovoljava potrebe sadašnjosti bez ugrožavanja mogućnosti budućih generacija da zadovolji vlastite potrebe. Ideja novih metrika je na neki način mjeriti koliko određena gospodarstva ulažu u održivost, odnosno koliko daju naglaska na očuvanje okoliša i prirodnih resursa. Podsjećamo da sve do sad povijesne metrike nisu razlikovale ekonomije koje su koristile obnovljive izvore energije naspram onih koje su koristile obnovljive ili pak ekonomije koje su aktivno pokušavale zaštiti okoliš, oceane i zrak naspram onih koje su spomenuto zagađivale.

Ljudsko blagostanje je vrlo vjerojatno do sad u indikatorima napretka bila najviše zanemarena komponenta jer je prilično nedodirljiva, a time i teško mjerljiva. Ona podrazumijeva koliko su ljudi zadovoljni s vlastitim životom kad u obzir uzmu količinu slobodnog vremena, osjećaj zajedništva u okolini kojoj žive, vlastito i zdravlje obitelji, zadovoljstvo ljubavnim životom, itd... Ovo su sve ne-ekonomski entiteti i pažnja im se počela pridavati relativno nedavno kada su psiholozi počeli naglašavati kako je količina novca koji ljudi zarađuju neočekivano mali faktor u ljudskoj ispunjenosti i sreći.

Svi ovi navedeni parametri pokušavaju se mjeriti kroz neke individualne indikatore od strane OECD (eng. \textit{Organisation for Economic Co-operation and Development}). Neki od tih indeksa uključuju: stope fertiliteta, migracije, postotak razvoda, stope pismenosti i zaposlenosti, očekivani životni vijek, težina dojenčadi, smrtnost dojenčadi, statistika konzumacije alkohola i opojnih droga, broj zatvora i prekršaja, automobilske i nesreće na radu, izlaznost na izborima i tako dalje... Uviđamo da je ovo poprilično širok spektar pojedinačnih indikatora te da je sada prava umjetnost odabrati najbitnije te ih na smislen način zajedno oblikovati u jedinstven broj ili pak u neku n-torku koja će jasno i nedvosmisleno govoriti o ukupnom ekonomskom, socijalnom i ekološkom napretku neke zemlje.
Upravo to postaje najveći problem novih metrika -- one pokušavaju uzeti u obzir mnoge stavke koje uistinu nisu zanemarive, ali na taj način izračun i interpretacija samog indeksa postaje znatno složenija u odnosu na primjerice BDP.


\chapter{Alternativni pokazatelji društvenog i ekonomkog napretka}

Nedostaci BDP-a poslužili su kao smislen usmjeritelj u oblikovanju obuhvatnijih i održivijih koncepata i potakla stvaranje raznolikosti alternativnih pokazatelja zamišljenih za adresiranje jednog ili više nedostataka prisutnih kod BDP-a. Na primjer, Eurostat nadgleda glavne kategorije pokazatelja održivog razvoja vezanih uz društveni i ekonomski razvoj, održivu potrošnju i proizvodnju, demografske promjene, javno zdravstvo, klimatske primjene i energiju, održivi transport, prirodne resurse, globalnu suradnju i dobro vođenje državne politike, ali svaki od njih razmatra posebno, a ne kao kompozitni pokazatelj. Osim toga, postoje brojni pokušaji razvijanje modela kompozitnih pokazatelja ekonomske dobrobiti, održivog razvoja, kakvoće života i blagostanja. Neke od njih razmatramo u ovom poglavlju. \citep{ieppmhw}

\section{Human Development Index (HDI)}
Ljudski razvojni indeks engleskog mnogo poznatijeg akronima HDI je jedan od, ako ne i trenutno najpoznatiji složeni pokazatelj ekonomskog i socijalnog napretka. Temelji se na tri pod-indikatora: 
\begin{itemize}
	\item bruto nacionalni dohodak \textit{per capita}
	\item indeks obrazovanja
	\item očekivani životni vijek.
\end{itemize}
Prva stavka bi trebala pokazivati zarađuju li stanovnici države dovoljno za barem prosječan standard življenja. Druga stavka pokazuje koliko je stanovništvo obrazovano te u kakvom je stanju obrazovanje zemlje. Treća stavka daje uvid u koliko je moguć dug i zdrav život u danoj zemlji.
Države koje imaju dugački period obrazovanja, dugačku očekivanu dob življenja te visok BDP \textit{per capita} će imati i visok HDI.

HDI je kao koncept razvijen '90-tih godina prošlog stoljeća od strane Programa Ujedinjenih naroda za razvoj (UNDP), konkretno razvio ga je pakistanski ekonomist Mahbub ul Haq te ga opisao u Godišnjem izvještaju spomenute agencije UNDP. Ideja je bila dakako skrenuti pozornost s ekomonije i pripadnih metrika fokusiranih na dohodak na ekonomiju i metrike koje su bliže čovjekovim potrebama. Na konceptu HDI-a radili su zajedno i drugi ekonomisti poput Paul Streetena, Frances Stewarta, Gustav Ranisa, Keith Griffina i Nobelovca Amartya Sena. Zanimljivo je to da je posljedni bio prilično skeptičan oko vrijednosti novoosmišljenog indeksa upravo zbog poteškoća izražavanja složene mjere poput indikatora ljudskog napretka isključivo jednim brojem unutar intervala $[0, 1]$.

Najrecentija metoda izračuna datira iz 2010. te se prema njoj HDI računa prema sljedećem izrazu: $$HDI = \sqrt[3]{LEI\cdot EI\cdot II} ,$$ gdje je $LEI$ indeks očekivane životne dobi, $EI$ je edukacioni indeks te $II$ predstavlja dohodovni indeks. Svaki od navedenih indeksa se računa jednostavnim izrazima.
Ako je $LE$ indeks očekivane životne dobi na rođenju, onda je: $$LEI = \frac{LE - 20}{85 - 20} ,$$ što znači da je $LEI = 1$ onda kad je očekivana životna dob na rođenju $LE = 85$.
Uvodimo srednji broj godina školovanja $MYS$ i očekivani broj godina školovanja $EYS$ koji se skaliranjem prevode u pripadne indekse $MYSI$ i $EYSI$ na sljedeći način: $$MYSI = \frac{MYS}{15},$$te: $$EYSI = \frac{EYS}{18} .$$ Sada se u konačnici indeks školovanja dobiva kao aritmetička sredina prethodna dva indeksa: $$EI = \frac{MYSI + EYSI}{2} .$$
Ako je $GNIpc$ bruto nacionalni dohodak po glavi tada se dohodovni indeks računa prema: $$II = \frac{\ln (GNIpc) - \ln (100)}{\ln (75 000) - \ln (100)} .$$ U slučaju da je $GNIpc = 75 000$ am. dolara dobivamo maksimalni dohodovni indeks, a minimalni u slučaju da je $GNIpc = 100$ dolara.

Prema izvješću UNDP-a iz 2014. prvih deset zemalja s najvišim HDI-om su Norveška, Australija, Švicarska, Nizozemska, SAD, Njemačka, Novi Zeland, Kanada, Singapur te Danska. Sve navedene zemlje imaju $HDI \geqslant 0.900$.
Hrvatska se nalazi na 47. mjestu s $HDI = 0.812$ čime se i dalje vodi kao razvijena zemlja.

Najveća kritika HDI-u upućena je iz razloga što ne obuhvaća ekološku komponentu, baš kao ni tehnološku. Uz to, podatci iz kojih se računaju spomenuti indeksi su često nedostupni ili netočni.


\section{Genuine Progess Indicator (GPI)}

GPI je mjera napretka osmišljena kako bi se zdravlje gospodarstva procjenjivalo obuhvatnije uključivanjem ekoloških i društvenih čimbenika koji nisu mjereni BDP-om. GPI je složena mjera koja se sastoji od 51 pokazatelja ekonomskog blagostanja, održivog razvoja, utjecaja na okoliš, društvene dobrobiti, što uključuje prihode od potrošnje, nejednakosti prihoda, potrošačke dugove, slabu zaposlenost, raspadanje obitelji, kriminal, kao i vrijednost neunovčavanog kućanskog, odgojnog i volonterskog posla. U GPI se uračunava ugrožavanje i uništavanje okoliša iscrpljivanjem prirodnih resursa, stvaranjem zagađenja, utjecanjem na klimatske promjene i drugim negativnim utjecajima na ekosustav. \citep{ieppmhw, wp_gpi}

GPI pokušava mjeriti je li utjecaj na okoliš ili društveni trošak proizvodnje i potrošnje u nekoj državi pozitivan ili negativan čimbenik u društvenom blagostanju. Uzimanjem u obzir troškova koje snosi društo u cjelini kako bi se ispravljalo ili upravljalo zagađenjem, siromaštvom i prosperitetom GPI uravnotežuje izdatke na vanjske troškove koji nisu izravno uključeni u cijenu koju BDP uračunava.\citep{wp_gpi}

Računanje GPI-ja se u pojednostavljenom obliku izražava na sljedeći način:
$$ GPI = A + B - C - D + I, $$
gdje je $A$ osobna potrošnja pomnožena faktorom ovisnom o prihodu, $B$ vrijednost netržišnih usluga koje pozitivno utječu na blagostanje, $C$ osobni trošak zbog pogoršanja stanja prirode, $D$ trošak štete na okoliš i prirodne resurse, a $I$ povećanje kapitalnih zaliha i bilance međunarodne trgovine

GPI pokazatelj je temeljan na ideji održive dobiti (John Hicks, 1948). Održiva dobit je količina koju osoba ili gospodarstvo može trošiti tijekom nekog razdoblja bez da mora smanjiti potrošnju tijekom sljedećeg razdoblja. Na isti način GPI oslikava stanje dobrobiti u društvu uzimajući u obzir mogućnost održavanja blagostanja na istoj razini u budućnosti.

Dok se BDP SAD-a gotovo utrostručio u drugoj polovici 20. stoljeća, GPI pokazuje 63\% od 1950. do 1970., a potom se stabilizirao i nije se puno mijenjao do 2000.. To je prikazano na slici \ref{fig:GDPvsGPI}. Razlika koja se javlja je uglavnom rezultat rasta marginalnog troška povezanog s najednakošću prihoda, iscrpljivanjem prirodnog kapitala, dugoročnim potrošačkim rashodima, vojnim rashodima, nepoželjnim nuspojavama rasta i stranim zaduživanjem. Usporedba BDP-a i GPI-ja služi kao argument da vođenje politike i odlučivanje temeljeno na upotrebi BDP-a možda prije bilo prikladno, ali sve više postaje neprikladno i kontraproduktivno. \citep{ieppmhw}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth]{GDPvsGPI}
	\caption{Usporedba BDP-a i GPI-a po glavi stanovnika u SAD-u u drugoj polovici 20. stoljeća}
	\label{fig:GDPvsGPI}
\end{figure}

GPI je izvrstan pokušaj procjenjivanja napretka temeljen na širokom rasponu pokazatelja vezanih uz blagostanje ljudi i kvalitetu života. Njegova glavna prednost je da se kao BDB izražava u novčanoj valuti i temelji na podacima potrošnje iz BDP-a. Međutim, kombiniranje ekonomskih, društvenih i ekoloških pokazatelje i pridjeljivanje novčane vrijednosti nemjerljivim i nedodirljivim čimbenicima umanjuje njegov kredibilitet stvarnog odraza životnih standarda.


\section{Index of Sustainable Economic Welfare (ISEW)}

ISEW je povijesni indikator ekonomskog razvoja koji je uz MEW indeks (Measure of Economic Welfare) iz 1972. i EAW indeks(Economic Aspects of Welfare) iz 1981. došao kao odgovor na uočene loše strane do tada isključivo korištenog GDP-a. ISEW je nastao 1989. iz umova Hermana Delya te Johna B. Cobba. Kao i drugi alternativni indikatori, ISEW ima dodane faktore koji uključuju ekološke troškove i troškove za naoružanje. Eksplicitni izraz za ISEW može se zapisati aproksimativno ovako: 

$ISEW$ = \emph{javni ne-obrambeni izdaci \emph- privatni obrambeni izdaci \emph+ investicije \emph+ usluge domaće radne snage \emph- troškovi ekološke degradacije \emph- redukcija prirodnog kapitala}.

Vremenom je dodavano još balansirajućih faktora indikatoru u priči pa je on kasnije prerastao u danas relativno poznat i korišten indeks GPI.


\chapter{Zaključak}

U ovom radu pokušali smo ukazati na važnost mjerenja, definiranja mjere i uloge koju mjere i pokazatelji imaju u napretku društva i opravdanost rada na poboljšanju postojećih ili razvijanju boljih mjera.

Sasvim je jasno da korištenjem loših mjera ekonomskog, socijalnog i ekološkog razvoja nitko neće imati dugoročne koristi. Vidjeli smo da jaka produkcija zemlje nikako ne znači zadovoljstvo ljudi i dobar odnos prema prirodi. Zapravo, vrlo često one države koje u nekom trenutku dožive uzlet u BDP-u znaju imati dosta nesretne građane i loše ekološke stanje zbog maksimalne eksploatacije sve u ime profita i brojke kojom se političari mogu hvaliti na sljedećim izborima.

Stvoriti novu metriku je danas nužno jer će slijepo praćenje BDP-a odvesti donositelje reformi i ekonomskih odluka na državnoj razini u krivom smjeru, u smjeru u kojem je fokus brojka, a ne ljudske potrebe. Naravno, osmišljavanje novih indikatora nije nimalo jednostavno jer je potrebno uzeti u obzir mnoge komponente koje su nedodirljive i teško mjerljive.
Ipak, već postoji iskustvo u korištenju alternativnih metrika poput HDI-a, GPI-a, ISEW-a u vidu zemalja poput Austrije, Finske, Nizozemske, Kanade, Francuske te savezne države Maryland te je ono samo pozitivno. Stoga, ne postoji razlog da se BDP kao relevantan indeks zauvijek ne napusti te se zamijeni prigodnijim indikatorima koji odgovaraju 21. stoljeću.

Daljnji napredak na području mjerenja socijalnog i ekonomskog napretka trebao bi biti u vidu standardiziranja nove mjere kako bi imali samo jedan indikator koji svi koriste te bi time bilo moguće raditi usporedbe između zemalja. Naravno, o najboljem modelu takve metrike trebalo bi još mnogo raspravljati, a još će iz same prakse biti vidljivo koji su nedostaci potencijalne alternative. Jedno je sigurno -- u slučaju da se zemlje svijeta što prije ne dogovore o novom načinu vrednovanja napretka ekonomije i društva teško da će održivi razvoj ikada biti drugo osim umne zamisli.


\bibliography{literatura}
\bibliographystyle{plain}

\end{document}
