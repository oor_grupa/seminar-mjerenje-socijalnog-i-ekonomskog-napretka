\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Ograni\IeC {\v c}enja BDP-a kao mjere napretka i dobrobiti}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Svojstva dobrih metrika napretka}{4}{chapter.3}
\contentsline {chapter}{\numberline {4}Alternativni pokazatelji dru\IeC {\v s}tvenog i ekonomkog napretka}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Human Development Index (HDI)}{7}{section.4.1}
\contentsline {section}{\numberline {4.2}Genuine Progess Indicator (GPI)}{9}{section.4.2}
\contentsline {section}{\numberline {4.3}Index of Sustainable Economic Welfare (ISEW)}{11}{section.4.3}
\contentsline {chapter}{\numberline {5}Zaklju\IeC {\v c}ak}{12}{chapter.5}
\contentsline {chapter}{\numberline {6}Literatura}{13}{chapter.6}
