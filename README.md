# Seminar: Mjerenje socijalnog i ekonomskog napretka #

## Pregled sadržaja ##
### Uvod ###
* 
* 

## Litaratura ##
* [FER-OOR: Ekonomski rast, razvoj i održivost](http://www.fer.unizg.hr/_download/repository/Rast_razvoj%5B2%5D.pdf)

* [**Measuring Economic Progress and Well-Being: How to move beyond GDP?**](http://www.oxfamamerica.org/static/media/files/measuring-economic-progress-and-well-being.pdf)

* [**Indicators of Economic Progress: The Power of Measurement and Human Welfare**](http://cadmusjournal.org/node/11)

* [The measurement of economic performance and social progress revisited: Reflections and Overview](http://spire.sciencespo.fr/hdl:/2441/5l6uh8ogmqildh09h4687h53k/resources/wp2009-33.pdf)

* [Wikipedia: Genuine progress indicator](https://en.wikipedia.org/wiki/Broad_measures_of_economic_progress)

* [Wikipedia: Broad measures of economic progress](https://en.wikipedia.org/wiki/Genuine_progress_indicator)

* [GNP](https://en.wikipedia.org/wiki/Green_national_product)

* [HDI](https://en.wikipedia.org/wiki/Human_Development_Index)

* [GPI](https://en.wikipedia.org/wiki/Genuine_progress_indicator)

* [The Growth Consensus Unravels](http://www.dollarsandsense.org/archives/1999/0799rowe.html)

* [Why Bigger Isn´t Better: The Genuine Progress Indicator - 1999 Update](http://users.nber.org/~rosenbla/econ302/lecture/GPI-GDP/gpi1999.pdf)